import logging
import time
import itertools

import numpy as np

log = logging.getLogger(__name__)


class StreamChunker(object):
    """ Takes a blob stream and creates a
    generator of 'chunks'. Each chunk
    is a dictionary with keys being a
    variable name and values being value arrays.
    """

    def __init__(self,
                 blob_stream,
                 data_spec,
                 chunk_size,
                 shuffle=False):
        self.blob_stream = blob_stream
        self.data_spec = data_spec
        self.chunk_size = chunk_size
        self.shuffle = shuffle
        self.stream_ended = False

    def __iter__(self):
        # note: chunker does not reset blob_stream.
        # TODO should it?
        def gen():
            while not self.stream_ended:
                buffers = np.empty(self.chunk_size, dtype=np.dtype(self.data_spec))
                # pull from blob_stream until we
                # fill chunk or blob_stream ends
                buffer_ix = 0

                tic = time.time()
                while buffer_ix < self.chunk_size:
                    try:
                        blob = next(self.blob_stream)
                        for key in buffers.dtype.names:
                            item = blob[key]
                            buffers[key][buffer_ix] = item
                        buffer_ix += 1
                    except StopIteration:
                        self.stream_ended = True
                        break
                log.info('chunk from stream: %f s', time.time()-tic)

                # don't bother returning empty buffer
                if buffer_ix == 0:
                    break
                elif buffer_ix < self.chunk_size:
                    # truncate buffers to actual number of instances.
                    # TODO pad to nearest batch size?
                    buffers = buffers[:buffer_ix]
                if self.shuffle and buffer_ix > 1:
                    np.random.shuffle(buffers)
                yield buffers
        return gen()
