
class BasePipeline(object):

    def build_train_source(self):
        raise NotImplementedError()

    def build_valid_source(self):
        raise NotImplementedError()

    def build_train_ops(self):
        raise NotImplementedError()

    def build_valid_ops(self):
        raise NotImplementedError()

    def build_test_ops(self):
        raise NotImplementedError()

