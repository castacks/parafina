""" A line section is a part of a pipeline.
"""

from __future__ import print_function
import traceback

from copy import deepcopy
from collections import OrderedDict
import time


class LineSection(object):

    def __init__(self, allow_overwrite=False):
        self.allow_overwrite = allow_overwrite
        self.ops = OrderedDict()
        self.callbacks = []

    def __setitem__(self, name, op):
        """ Note: overwrites op.name with name.
        """
        if not self.allow_overwrite and (name in self.ops):
            raise ValueError('Name {} is taken.'.format(name))
        op.name = name
        self.ops[name] = op

    def __getitem__(self, key):
        return self.ops[key]

    def add_op(self, op, force_original_name=False):
        """ By default, will mutate names to avoid collisions.
        """
        if force_original_name:
            name = op.name
        else:
            name = (op.name or 'op') + '%04d'%(len(self.ops))
        self[name] = op

    def print_mappings(self):
        for opname, op in self.ops.iteritems():
            print('{}:'.format(opname))
            for k, v in getattr(op, 'mapping', {}):
                print('\t{} -> {}'.format(k, v))

    def add_callback(self, cb):
        self.callbacks.append(cb)

    def apply_no_cb(self, blob):
        for opname, op in self.ops.iteritems():
            op(blob)
            if len(blob) == 0:
                break

    def apply(self, blob):
        """ We assume blobs are mutated.
        """

        if len(self.callbacks) == 0:
            self.apply_no_cb(blob)
            return

        # callbacks for input
        for cb in self.callbacks:
            cb.init(blob)

        for opname, op in self.ops.iteritems():
            for cb in self.callbacks:
                cb.pre_op(opname, blob)

            op(blob)

            # convention: skip empty blobs
            if len(blob) == 0:
                break

            for cb in self.callbacks:
                cb.post_op(opname, blob)

    def debug_apply(self, blob):
        """ create a snapshot of blobs as it passes through pipeline.
        useful for debugging.
        TODO consider using callback
        """

        blobs = OrderedDict()
        blobs['src'] = deepcopy(blob)
        try:
            for opname, op in self.ops.iteritems():
                op(blob)
                blobs[opname] = deepcopy(blob)
        except Exception as ex:
            print('stopping on error for op: {}, error: {}'.format(opname, traceback.format_exc()))
        return blobs

    def __call__(self, blob):
        return self.apply(blob)


class LineSectionCallback(object):

    def init(self, blob):
        pass

    def pre_op(self, opname, blob):
        pass

    def post_op(self, opname, blob):
        pass


class ProfileOpCallback(LineSectionCallback):

    def __init__(self):
        self.tic = None
        self.toc = None

    def pre_op(self, opname, blob):
        self.tic = time.time()

    def post_op(self, opname, blob):
        self.toc = time.time()
        print('{} took {:.2f} ms'.format(opname, 1000*(self.toc-self.tic)))


class PdbCallback(LineSectionCallback):

    def __init__(self, init=False, pre=False, post=True):
        self.on_init = init
        self.on_pre = pre
        self.on_post = post

    def init(self, blob):
        if self.on_init:
            print('init')
            import pdb
            pdb.set_trace()

    def pre_op(self, opname, blob):
        if self.on_pre:
            print('pre', opname)
            import pdb
            pdb.set_trace()

    def post_op(self, opname, blob):
        if self.on_post:
            print('post', opname)
            import pdb
            pdb.set_trace()
