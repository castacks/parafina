import logging
import numpy as np

import cPickle as pickle
import zmq

log = logging.getLogger(__name__)


def recv_array(socket, flags=0, copy=True, track=False):
    """ recv numpy array """
    md = socket.recv_json(flags=flags)
    msg = socket.recv(flags=flags, copy=copy, track=track)
    buf = buffer(msg)
    A = np.frombuffer(buf, dtype=md['dtype'])
    return A.reshape(md['shape'])


def send_array(socket, A, flags=0, copy=True, track=False):
    """send a numpy array with metadata"""
    md = dict(dtype=str(A.dtype), shape=A.shape)
    socket.send_json(md, flags | zmq.SNDMORE)
    return socket.send(A, flags, copy=copy, track=track)


def zmq_ventilator_aux(bgen, hwm, port):
    """ pulls stuff from bgen and pushes it into port
    """
    ctx = zmq.Context()
    push_sock = ctx.socket(zmq.PUSH)
    push_sock.set_hwm(hwm)
    push_sock.bind('tcp://127.0.0.1:%s' % port)
    # If it gets stopiteration it will merely forward it
    # for this to work, bgen must restart each time iter() is called on it
    while True:
        try:
            for blob in bgen:
                packed = pickle.dumps(blob, pickle.HIGHEST_PROTOCOL)
                push_sock.send(packed)
        except StopIteration, ex:
            push_sock.send(ex)
    # not really reachable
    push_sock.close()


def zmq_op_aux(ops, hwm, src_port, dst_port):
    """ pulls from src_port, applies ops and pushes to dst_port
    """
    ctx = zmq.Context()
    pull_sock = ctx.socket(zmq.PULL)
    pull_sock.set_hwm(hwm)
    # TODO connect or bind?
    pull_sock.connect('tcp://127.0.0.1:%d' % src_port)

    push_sock = ctx.socket(zmq.PUSH)
    push_sock.set_hwm(hwm)
    push_sock.connect('tcp://127.0.0.1:%d' % dst_port)

    # infinite loop. if it see stopiteration it will merely forward it
    while True:
        if pull_sock.closed:
            log.fatal('pull_sock %d for consumer is closed', pull_sock)
            return

        blob_or_ex = pickle.loads(pull_sock.recv())
        if isinstance(blob_or_ex, StopIteration):
            push_sock.send(pickle.dumps(blob_or_ex, pickle.HIGHEST_PROTOCOL))
            continue

        blob = blob_or_ex
        ops(blob)
        if len(blob) == 0:
            continue

        push_sock.send(pickle.dumps(blob, pickle.HIGHEST_PROTOCOL))


def zmq_ventilator_and_op_aux(bgen, ops, hwm, dst_port):
    """ pulls stuff from src, runs ops and pushes it into port
    most useful when only two workers are desired
    """
    ctx = zmq.Context()
    push_sock = ctx.socket(zmq.PUSH)
    push_sock.set_hwm(hwm)
    push_sock.bind('tcp://127.0.0.1:%s' % dst_port)

    while True:
        try:
            for blob in bgen:
                ops(blob)
                if len(blob) == 0:
                    continue
                push_sock.send(pickle.dumps(blob, pickle.HIGHEST_PROTOCOL))
        except StopIteration, ex:
            push_sock.send(ex)
    # not really reachable
    push_sock.close()
