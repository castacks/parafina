__all__ = ['ZmqPump']

import logging

from multiprocessing import Process
import cPickle as pickle
import zmq

from .stream_chunker import StreamChunker
from .zmq_util import (zmq_op_aux,
                       zmq_ventilator_aux,
                       zmq_ventilator_and_op_aux)


log = logging.getLogger(__name__)


class ZmqPump(object):
    """ A source of blobs.
    Runs in multiple processes to try to improve CPU usage.
    Uses ZMQ pipeline pattern.
    """

    def __init__(self,
                 src,
                 ops,
                 workers=2,
                 worker_port=5558,
                 queue_size=10):

        self.src = src
        self.ops = ops
        self.queue_size = queue_size

        self.workers = workers
        self.worker_port = 5558
        self.ventilator_port = self.worker_port+1

        if workers == 1:
            raise ValueError('workers should be >= 2')
        elif workers == 2:
            # one worker to do source and ops
            self.ventilator = Process(target=zmq_ventilator_and_op_aux,
                                      args=(self.src,
                                            self.ops,
                                            queue_size,
                                            self.worker_port))
            self.ventilator.start()
        elif workers > 2:
            # one ventilator worker, n-1 consumer workers
            # launch consumer workers
            for _ in range(workers-1):
                proc = Process(target=zmq_op_aux,
                               args=(self.ops,
                                     queue_size,
                                     self.ventilator_port,
                                     self.worker_port))
                proc.start()

            # spawn src/ventilator
            self.ventilator = Process(target=zmq_ventilator_aux,
                                      args=(self.src,
                                            queue_size,
                                            self.ventilator_port))

            self.ventilator.start()

        # set up port to get results
        self.ctx = zmq.Context()
        self.pull_sock = self.ctx.socket(zmq.PULL)
        self.pull_sock.set_hwm(queue_size)
        # self.pull_sock.bind('tcp://127.0.0.1:%d'%self.worker_port)
        self.pull_sock.connect('tcp://127.0.0.1:%d'%self.worker_port)

    def blob_gen(self):
        """ Call all ops in sequence.
        Skips empty blobs (can be used for filtering).
        StopIteration exceptions are not handled (i.e.  they propagate).
        """
        def gen():
            while True:
                blob_or_ex = pickle.loads(self.pull_sock.recv())
                if isinstance(blob_or_ex, StopIteration):
                    break
                yield blob_or_ex
        return gen()

    def chunked_array_gen(self, data_spec, chunk_size):
        bgen = self.blob_gen()
        cgen = StreamChunker(bgen, data_spec, chunk_size)
        return cgen
