__all__ = ['SerialPump']


from .stream_chunker import StreamChunker


class SerialPump(object):
    """ A source of blobs.
    Runs in single process.
    """

    def __init__(self, src, ops):
        self.src = src
        self.ops = ops

    def blob_gen(self):
        """ Call all ops in sequence.
        Skips empty blobs (can be used for filtering).
        StopIteration exceptions are not handled (i.e.  they propagate).
        """
        def gen():
            for blob in self.src:
                self.ops(blob)
                if len(blob) == 0:
                    continue
                yield blob
        return gen()

    def chunked_array_gen(self, data_spec, chunk_size):
        bgen = self.blob_gen()
        cgen = StreamChunker(bgen, data_spec, chunk_size)
        return cgen
