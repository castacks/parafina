""" Ops that transform from and to ROS messages.
You need to have ROS for this to work.
(In particular, rospy and sensor_msgs).
"""

import cv2
import numpy as np
import traitlets as tr

try:
    import cv_bridge
    from sensor_msgs.msg import CompressedImage
except ImportError:
    cv_bridge = None
    CompressedImage = None

from .base import ParafinaOp


def my_bridge(msg, desired_encoding='bgr8'):
    """ outputs to mono8 if input is mono8, else bgr """

    if desired_encoding != 'bgr8':
        raise ValueError('only bgr8 supported')

    buf = np.frombuffer(msg.data, dtype=np.uint8)
    if 'CompressedImage' in str(type(msg)):
        # compressed image will output to bgr8
        img_np = cv2.imdecode(buf, -1)  # LOAD_IMAGE_UNCHANGED
        if 'bayer_rggb8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_BG2BGR)
        elif 'bayer_bggr8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_RG2BGR)
        elif 'bayer_gbrg8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GR2BGR)
        elif 'bayer_grbg8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GB2BGR)

    else:
        if msg.encoding == 'mono8':
            img_np = buf.reshape((msg.height, msg.width))
        elif msg.encoding == 'rgb8':
            img_np = buf.reshape((msg.height, msg.width, 3))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_RGB2BGR)
        elif msg.encoding in ('bgr8', '8UC3'):
            img_np = buf.reshape((msg.height, msg.width, 3))
        elif (msg.encoding == 'bayer_rggb8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_BG2BGR)
        elif (msg.encoding == 'bayer_bggr8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_RG2BGR)
        elif (msg.encoding == 'bayer_gbrg8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GR2BGR)
        elif (msg.encoding == 'bayer_grbg8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GB2BGR)
        else:
            raise ValueError("can't handle this encoding yet: {}".format(msg.encoding))
    return img_np


class MessageToImage(ParafinaOp):

    desired_encoding = tr.Unicode('bgr8')
    use_cv_bridge = tr.Bool(False)
    mapping_default = {'rgb_msg': 'rgb_image'}

    def _post_init(self):
        if self.use_cv_bridge:
            self._bridge = cv_bridge.CvBridge()
            self._bridge_fn = self._bridge.imgmsg_to_cv2
        else:
            self._bridge_fn = my_bridge

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            img_msg = blob[k_in]
            img = self._bridge_fn(img_msg, desired_encoding=self.desired_encoding)
            blob[k_out] = img


class ImageToMessage(ParafinaOp):

    desired_encoding = tr.Unicode('passthrough')
    compression = tr.Unicode('none')
    mapping_default = {'rgb_image': 'rgb_msg'}

    def _post_init(self):
        self.bridge = cv_bridge.CvBridge()

        if self.compression not in ('jpg', 'png', 'none'):
            raise ValueError('compression must be jpg/png/none')

        if self.compression == 'jpg':
            self.cv_format = '.jpg'
            self.msg_format = 'jpeg'
        elif self.compression == 'png':
            self.cv_format = '.png'
            self.msg_format = 'png'

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            img = blob[k_in]
            if self.compression == 'none':
                msg = self.bridge.cv2_to_imgmsg(img, encoding=self.desired_encoding)
            else:
                msg = CompressedImage()
                msg.format = self.msg_format
                ret, buf = cv2.imencode(self.cv_format, img)
                if not ret:
                    raise IOError('error encoding image with cv2')
                msg.data = np.asarray(buf, dtype=np.uint8).tostring()
            if 'stamp' in blob.keys():
                msg.header.stamp = blob['stamp']
            blob[k_out] = msg
