
import traitlets as tr


class ParafinaOp(tr.HasTraits):
    """ An Op is something that mutates a dict-like object, i.e. a "blob".
    """

    debug = tr.Bool(False)
    name = tr.CUnicode()
    mapping = tr.Dict()

    def __init__(self, mapping=None, **kwargs):
        """ initialize op.
        """
        # defaults for base traitlets
        for trait_name in ('debug',
                           'name',
                           'mapping'):
            trait_value = getattr(self, trait_name+'_default', None)
            if trait_value is not None:
                setattr(self, trait_name, trait_value)

        # override default values
        if mapping is not None:
            self.mapping = mapping
        self.debug = kwargs.pop('debug', False)
        self.name = kwargs.pop('name', self.__class__.__name__)

        # set the rest of values, validating they are traits
        for k, v in kwargs.items():
            if k not in self.trait_names():
                raise ValueError('Unknown parameter: {}'.format(k))
            setattr(self, k, v)

        self._post_init()

    def _post_init(self):
        """ Called at end of __init__. Good for validation and other setup,
        such as dynamically setting variables.
        """
        pass

    def _update(self, blob):
        """ Should do the work. If blob is empty it will be skipped.
        blob will usually be mutated in place.
        """
        pass

    def __call__(self, blob):
        if self.debug:
            import ipdb
            ipdb.set_trace()
        return self._update(blob)
