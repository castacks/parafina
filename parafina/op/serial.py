"""
Loading from common serialization formats
"""

import json
import cPickle as pickle

from .base import ParafinaOp

import logging


log = logging.getLogger(__name__)

class JsonLoad(ParafinaOp):

    mapping_default = {'json_fname': 'json_data'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            with open(blob[kin], 'r') as f:
                blob[kout] = json.load(f)


class PickleLoad(ParafinaOp):

    mapping_default = {'pickle_fname': 'pickle_data'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            with open(blob[kin], 'r') as f:
                blob[kout] = pickle.load(f)
