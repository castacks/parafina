import logging

import numpy as np
import traitlets as tr

import cv2
from PIL import Image
from PIL import ImageEnhance
from PIL import ImageFilter

try:
    import skimage.exposure
    import skimage.data
except ImportError, ex:
    skimage = None

try:
    import colorcorrect.algorithm as ccalg
    # import colorcorrect.util as ccutil
except ImportError, ex:
    ccalg = None

from .base import ParafinaOp

log = logging.getLogger(__file__)


class RectangleMask(ParafinaOp):
    prob = tr.Float(0.2)
    max_scale = tr.Float(0.5)

    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin].copy()
            max_w = int(self.max_scale * img.shape[0])
            max_h = int(self.max_scale * img.shape[1])
            cx = np.random.randint(2, img.shape[1]-2)
            cy = np.random.randint(2, img.shape[0]-2)
            w = np.random.randint(2, max_w)
            h = np.random.randint(2, max_h)
            x0 = np.clip(int(cx-w/2), 0, img.shape[1]-1)
            y0 = np.clip(int(cy-h/2), 0, img.shape[0]-1)
            x1 = np.clip(int(cx+w/2), x0+1, img.shape[1]-1)
            y1 = np.clip(int(cy+h/2), y0+1, img.shape[0]-1)
            if ((np.random.random() < self.prob) and
                (x1 > x0) and (y1 > y0)):
                img[y0:y1, x0:x1] = 0
            blob[kout] = img

class BlobMask(ParafinaOp):
    prob = tr.Float(0.2)
    max_scale = tr.Float(0.5)
    fraction = tr.Float(0.2)

    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin].copy()

            h, w = img.shape[0], img.shape[1]
            sz = max(h, w)

            if (np.random.random() < self.prob):
                blobs = skimage.data.binary_blobs(sz, self.max_scale, volume_fraction=self.fraction)
                blobs = (blobs[0:h, 0:w])
                img[blobs] = 0
            blob[kout] = img


class ColorToGray(ParafinaOp):
    # TODO should it be a different name?
    mapping_default = {'rgb_image': 'rgb_image'}
    rgb_or_bgr = tr.CUnicode('rgb')

    def _post_init(self):
        if self.rgb_or_bgr not in ('rgb', 'bgr'):
            raise ValueError('rgb_or_bgr should be "rgb" or "bgr"')

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]
            if self.rgb_or_bgr == 'rgb':
                gimg = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            else:
                gimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


class ColorCorrect(ParafinaOp):

    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]
            blob[kout] = ccalg.grey_world(img)
            # blob[kout] = ccalg.retinex(img)
            # blob[kout] = ccalg.retinex_adjust(img)


class ApplyPhotoJitter(ParafinaOp):
    """
    TODO look at tensorflow ops
    TODO customization
    """

    # NOTE. ace equalization (color_correct1) is quite slow.
    # TODO an issue is whether input is rgb or bgr --
    # for now we assume rgb
    weights_algorithms = tr.Any([(0.00, 'color_correct1'),
                                 (0.08, 'color_correct2'),
                                 (0.08, 'color_correct3'),
                                 (0.08, 'color_correct4'),
                                 (0.08, 'color_correct5'),
                                 (0.1, 'color_enhance'),
                                 (0.2, 'hue_jitter'),
                                 (0.3,  'pass_through')])

    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        if not ccalg:
            raise Exception('colorcorrect needed for ApplyPhotoJitter')
        if not skimage:
            raise Exception('skimage needed for ApplyPhotoJitter')

        weights, algorithms = zip(*self.weights_algorithms)

        self.weights = np.asarray(weights)/np.sum(weights)
        self.algorithms = algorithms

    def color_correct1(self, img):
        return ccalg.automatic_color_equalization(img)

    def color_correct2(self, img):
        return ccalg.grey_world(img)

    def color_correct3(self, img):
        # return ccalg.standard_deviation_weighted_grey_world(ccutil.from_pil(img)))
        return ccalg.max_white(img)

    def color_correct4(self, img):
        return ccalg.retinex_adjust(img)

    def color_correct5(self, img):
        return ccalg.retinex(img)

    def color_enhance(self, img):
        # 0.0 = grayscale
        # 1.0 = original image
        # 3.0-4.0 = oversaturated
        enhancer = ImageEnhance.Color(Image.fromarray(img))
        # iimg = enhancer.enhance(np.random.uniform(0.2, 1.8))
        iimg = enhancer.enhance(np.random.triangular(0.2, 1.0, 2.2))
        return np.asarray(iimg)

    def hue_jitter(self, img):
        # based on pytorch
        # limits are +/-0.5 (which are the same due to wraparound)
        hue_factor = np.random.triangular(-0.10, 0.0, 0.10)
        iimg = Image.fromarray(img)
        h, s, v = iimg.convert('HSV').split()
        np_h = np.array(h, dtype='u1')
        with np.errstate(over='ignore'):
            np_h += np.uint8(hue_factor * 255)
        h = Image.fromarray(np_h, 'L')
        iimg = Image.merge('HSV', (h, s, v)).convert('RGB')
        return np.asarray(iimg)

    def pass_through(self, img):
        return img

    def _update(self, blob):
        for kin, kout in self.mapping.items():

            img = blob[kin]
            if img.max() == 0:
                blob[kout] = img
                continue

            alg_name = np.random.choice(self.algorithms, p=self.weights)
            alg = getattr(self, alg_name)

            # log.info('color {}'.format(alg))
            try:
                blob[kout] = alg(img)
            except Exception as ex:
                log.warn('error in ApplyPhotoJitter %r', ex)
                blob[kout] = img

            # yield wimg.clip(0,255).astype(np.uint8), wlimg.clip(0,255).astype(np.uint8)



class ApplyGrayPhotoJitter(ParafinaOp):
    """
    TODO look at tensorflow ops
    TODO customization
    """
    weights_algorithms = tr.Any([(0.28,  'gamma_adjust'),
                                 (0.08, 'sigmoid_adjust'),
                                 (0.2, 'brightness_enhance'),
                                 (0.1, 'sharpness_enhance'),
                                 (0.2, 'contrast_enhance'),
                                 (0.08, 'gaussian_blur'),
                                 (0.4,  'pass_through')])

    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        if not skimage:
            raise Exception('skimage needed for ApplyGrayPhotoJitter')

        weights, algorithms = zip(*self.weights_algorithms)

        self.weights = np.asarray(weights)/np.sum(weights)
        self.algorithms = algorithms

    def gamma_adjust(self, img):
        return skimage.exposure.adjust_gamma(img,
                # np.random.uniform(0.6, 1.8))
                np.random.triangular(0.2, 1.0, 2.6))


    def sigmoid_adjust(self, img):
        return skimage.exposure.adjust_sigmoid(img,
                cutoff=np.random.uniform(0.3, 0.45),
                gain=np.random.randint(4, 10))

    def brightness_enhance(self, img):
        enhancer = ImageEnhance.Brightness(Image.fromarray(img))
        # iimg = enhancer.enhance(np.random.uniform(0.6, 1.4))
        iimg = enhancer.enhance(np.random.triangular(0.2, 1.0, 2.4))
        return np.asarray(iimg)

    def contrast_enhance(self, img):
        enhancer = ImageEnhance.Contrast(Image.fromarray(img))
        # iimg = enhancer.enhance(np.random.uniform(0.6, 1.4))
        iimg = enhancer.enhance(np.random.triangular(0.2, 1.0, 2.8))
        return np.asarray(iimg)

    def sharpness_enhance(self, img):
        enhancer = ImageEnhance.Sharpness(Image.fromarray(img))
        iimg = enhancer.enhance(np.random.uniform(0.8, 1.2))
        return np.asarray(iimg)

    def gaussian_blur(self, img):
        # TODO adjust to size of image?
        iimg = Image.fromarray(img.copy())
        iimg = iimg.filter(ImageFilter.GaussianBlur(np.random.triangular(0.5, 1.0, 2.5)))
        return np.asarray(iimg)

    def pass_through(self, img):
        return img

    def _update(self, blob):
        for kin, kout in self.mapping.items():

            img = blob[kin]
            if img.max() == 0:
                blob[kout] = img
                continue

            alg_name = np.random.choice(self.algorithms, p=self.weights)
            # print(alg_name)
            alg = getattr(self, alg_name)

            try:
                blob[kout] = alg(img)
            except Exception as ex:
                log.warn('error in ApplyGrayPhotoJitter %r', ex)
                blob[kout] = img
