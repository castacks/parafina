
import numpy as np
import traitlets as tr

from .base import ParafinaOp


class KeysToVec(ParafinaOp):
    """ pull out vectors from a dictionary inside the blob.
    the chosen vectors are determined by keys.
    """
    keys = tr.List(['a', 'b', 'c'])
    dtype = tr.Unicode('float32')
    mapping_default = {'line_dict': 'y'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            d = blob[kin]
            abc = [d.get(k, None) for k in self.keys]
            blob[kout] = np.array(abc, dtype=self.dtype)


class VecScaleOffset(ParafinaOp):
    """ scale and offset vectors, according
    to x_i = scale*(x_i + offset).
    """
    scales = tr.Any()
    offsets = tr.Any()
    mapping_default = {'x': 'x'}

    def _post_init(self):
        # TODO use traitlets for validation
        assert(len(self.scales) > 0)
        assert(len(self.offsets) > 0)
        assert(len(self.scales) == len(self.offsets))

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            for i, (scale, offset) in enumerate(zip(self.scales, self.offsets)):
                x[i] = scale*(x[i]+offset)
            blob[kout] = x


class ScaleOffset(ParafinaOp):
    scale = tr.Float(1.)
    offset = tr.Float(0.)
    mapping_default = {'x': 'x'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            blob[kout] = x*self.scale + self.offset


class UnsqueezeFirstDim(ParafinaOp):
    """ Adds an extra singleton dimension in first dim.
    Useful to add singleton channel or singleton batch dim.
    TODO: generalize
    """

    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            x = x[None, ...]
            blob[kout] = x


class Clone(ParafinaOp):
    mapping_default = {'rgb_image': 'rgb_image_copy'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            blob[kout] = x


class Clip(ParafinaOp):
    minval = tr.Any()
    maxval = tr.Any()
    mapping_default = {'x': 'x'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            x = x.clip(self.minval, self.maxval)
            blob[kout] = x


class Concatenate(ParafinaOp):
    mapping_default = {('x', 'y'): 'xy'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin[0]].copy()
            y = blob[kin[1]].copy()
            if x.ndim == 2:
                x = x[..., None]
            if y.ndim == 2:
                y = y[..., None]
            xy = np.concatenate((x, y), 2)
            blob[kout] = xy
