
import logging
import numpy as np
import traitlets as tr
import skimage.transform as skimgtf

from ..util import lines

from .base import ParafinaOp


log = logging.getLogger(__name__)


class WarpLines(ParafinaOp):
    """ warps lines according to a transformation,
    fixes endpoints and abc coeffcients
    always_left_right: always make points left to right.
    this gets messed up e.g. after flipping.

    TODO: this is hackish. use projective maths.
    """

    tf_key = tr.CUnicode('rgb_tf')
    always_left_right = tr.Bool(True)
    mapping_default = {'line_dict': 'line_dict'}

    def _update(self, blob):
        kin, kout = self.mapping.items()[0]
        line_dict = blob[kin]
        imgtf_dict = blob[self.tf_key]
        # get tf object
        w, h = imgtf_dict['w'], imgtf_dict['h']
        tfmat = imgtf_dict['matrix']
        tfobj = skimgtf.AffineTransform(matrix=tfmat)

        # original endpoints
        xs, ys = line_dict['xs'], line_dict['ys']

        # transform endpoints. note that tf is the INVERSE
        # so the inverse is the forward tf
        xy0 = tfobj.inverse((xs[0], ys[0]))
        xy1 = tfobj.inverse((xs[1], ys[1]))
        if self.always_left_right and xy0.flat[0] > xy0.flat[1]:
            wxys = np.r_[xy1, xy0]
        else:
            wxys = np.r_[xy0, xy1]
        wxs, wys = wxys[:, 0], wxys[:, 1]

        # the endpoints may be outside the image. clip them
        # fxsfys = lines.fix_endpoints(wxs, wys, w, h)
        fxsfys = lines.fix_endpoints(wxs, wys, w, h)
        if fxsfys is None:
            # log.warn('bad blob: {} {} {} {}'.format(wxs, wys, w, h))
            blob['BAD'] = True
            return
        fxs, fys = fxsfys

        # recompute the abc coefficients
        fa, fb, fc = lines.endpoints_to_abc(fxs, fys)

        fr, ftheta = lines.abc_to_rhotheta(fa, fb, fc)
        if np.imag(ftheta) != 0.:
            log.warning('theta is complex: {}'.format(ftheta))
        ftheta = np.real(ftheta)

        if np.isnan(fa):
            # print w, h, fxs, fys, wxs, wys, fa, fb, fc
            import ipdb; ipdb.set_trace()

        line_dict['a'] = fa
        line_dict['b'] = fb
        line_dict['c'] = fc
        line_dict['xs'] = fxs
        line_dict['ys'] = fys
        line_dict['rho'] = fr
        line_dict['theta'] = ftheta

        blob[kout] = line_dict


class RhoThetaPreprocess(ParafinaOp):
    """ TODO just make a scaler+offset obj
    TODO mapping
    """

    y_key = tr.Unicode('horz_dict')
    rho_scale = tr.CFloat(1./227)
    rho_offset = tr.CFloat(113.5)
    theta_scale = tr.CFloat(1./np.pi)
    theta_offset = tr.CFloat(0.)
    mapping_default = {'horz_dict': 'horz_dict'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            horz_dict = blob[k_in]
            horz_dict['rho'] = self.rho_scale*(horz_dict['rho']+self.rho_offset)
            horz_dict['theta'] = self.theta_scale*(horz_dict['theta']+self.theta_offset)
            blob[k_out] = horz_dict
