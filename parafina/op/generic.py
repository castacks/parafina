import traitlets as tr

from .base import ParafinaOp


class LambdaOp(ParafinaOp):

    fn = tr.Any()

    @tr.validate('fn')
    def validate_fn(self, proposal):
        """ ensure fn is callable """
        if not callable(proposal.value):
            raise tr.TraitsError('need callable')
        return proposal.value

    def _update(self, blob):
        self.fn(blob)
