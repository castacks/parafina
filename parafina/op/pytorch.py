import numpy as np
import torch
import traitlets as tr

from .base import ParafinaOp


class NumpyToTorch(ParafinaOp):
    mapping_default = {'x': 'x'}

    # option to cast variables.
    # keys refer to OUT variables.
    # use 'torch.LongTensor', 'torch.FloatTensor'
    type_cast_map = tr.Dict()

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            x = torch.from_numpy(x)
            if kout in self.type_cast_map:
                x = x.type(self.type_cast_map[kout])
            blob[kout] = x
