"""
Image processing and jittering
TODO add caching
"""

import logging
import numpy as np
import traitlets as tr
import cv2

# for similarity transform
import skimage.transform as skimgtf

from ..util.img_dims import ImgDims
from ..util import image_jitter as imgjit

from .base import ParafinaOp


log = logging.getLogger(__file__)


def aspect_preserving_scale(img, w, h, interp=cv2.INTER_LINEAR):
    """
    adjusts either w or h depending on which is None

    :parameters:
        - interp: int
            interpolation code from PIL.Image
    """
    old_h, old_w = img.shape[:2]
    ratio = float(old_w)/old_h
    if h is None:
        h = int(w/ratio)
    elif w is None:
        w = int(h*ratio)
    return cv2.resize(img, (w, h), None, 0, 0, interp)


def get_cv2_interp_code(interp):
    inter_name = 'INTER_{}'.format(interp.upper())
    try:
        return getattr(cv2, inter_name)
    except AttributeError:
        raise ValueError('Interpolation method {} not found.'.format(interp))


class ApplyImageTfs(ParafinaOp):
    """
    """

    interp = tr.CUnicode('linear')
    # this one uses linear interpolation+thresholding
    pseudo_max_interp = tr.Bool(False)
    dtype = tr.Unicode('float32')
    mapping_default = {('rgb_image', 'rgb_tf'): 'rgb_image'}

    def _post_init(self):
        self._interp = get_cv2_interp_code(self.interp)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img, imgtf = blob[kin[0]], blob[kin[1]]
            if self.dtype is not None:
                img = img.astype(self.dtype)
            M = imgtf['matrix']
            # print M
            assert(img.ndim == 2 or img.ndim == 3)

            # warptf = self.get_tf(img.shape)
            # returns in uint8 or float.
            # tell cv2 we are giving it the inverse map.

            flags = self._interp | cv2.WARP_INVERSE_MAP
            border_mode = cv2.BORDER_REFLECT
            wimg = cv2.warpAffine(img,
                                  M[:2],
                                  (imgtf['w'], imgtf['h']),
                                  None,
                                  flags,
                                  border_mode)

            blob[kout] = wimg


class ComputeScaleTf(ParafinaOp):
    """
    Compute a scale transform, but do NOT apply it.
    """

    target_h = tr.CInt(480)
    mapping_default = {'rgb_image': 'rgb_tf'}

    def _update(self, blob):
        for k_in, k_out in self.mapping:
            img = blob[k_in]
            src_dim = ImgDims.from_shape(img.shape)
            w, h = src_dim.width, src_dim.height
            ratio = float(w)/h
            if h is None:
                h = (w/ratio)
            elif w is None:
                w = (h*ratio)
            sx = src_dim.width/w
            sy = src_dim.height/h
            scale_tf = skimgtf.SimilarityTransform(scale=(sx, sy))
            blob[k_out] = {'w': w,
                           'h': h,
                           'tf_matrix': scale_tf.params}


class SampleGeometricJitterTf(ParafinaOp):
    """ Creates a randomized affine tf warp
    """

    translation_range_w_px = tr.Tuple((-40, 40))
    translation_range_h_px = tr.Tuple((-10, 10))
    rotation_range_deg = tr.Tuple((0, 0))
    zoom_range = tr.Tuple((1., 1.))
    do_flip = tr.Bool(False)
    mapping_default = {'_': 'geom_jit_params'}

    def _post_init(self):
        self.param_sampler = imgjit.AffineTfParamsSampler(
                rotation_range_deg=self.rotation_range_deg,
                zoom_range=self.zoom_range,
                translation_range_px=(self.translation_range_w_px,
                                      self.translation_range_h_px),
                do_flip=self.do_flip
                )

    def _update(self, blob):
        for k, v in self.mapping.items():
            aff_tf_params = self.param_sampler()
            blob[v] = aff_tf_params._asdict()


class ComputeImageTfs(ParafinaOp):
    """
    (conceptual) operations:
    - center around origin
    - scale to pre_scale_shape
    - apply affine tf (rotation/zoom/shift)
    - undo centering
    - crop to crop_to_shape
    - scale to post_scale_shape
    TODO: noop for cases when no tf wanted

    """

    tf_key = tr.CUnicode('geom_jit_params')
    pre_scale_shape = tr.Any()
    crop_to_shape = tr.Any()
    post_scale_shape = tr.Any()
    preserve_aspect_ratio = tr.Bool(True)
    debug = tr.Bool(False)
    mapping_default = {'rgb_image': 'rgb_tf'}

    def _post_init(self):
        # probably should have smarter logic here
        self.pre_scale_dims = ImgDims.from_shape(self.pre_scale_shape[-2:])
        self.crop_to_dims = ImgDims.from_shape(self.crop_to_shape[-2:])
        self.post_scale_dims = ImgDims.from_shape(self.post_scale_shape[-2:])

    def _update(self, blob):
        kin, kout = self.mapping.items()[0]

        img = blob[kin]

        if img.size == 0:
            log.warn('image is size 0, clearing blob')
            blob.clear()
            return

        img_dims = ImgDims.from_shape(img.shape)

        if self.preserve_aspect_ratio:
            prescale_dims, prescale_tf = imgjit.build_aspect_ratio_preserving_tf2(
                    img_dims, self.pre_scale_dims)
        else:
            prescale_tf = imgjit.build_scale_tf(img_dims, self.pre_scale_dims)
            prescale_dims = self.pre_scale_dims

        # (un)centering is around prescale dimensions
        center_tf, uncenter_tf = imgjit.build_center_uncenter_tfs(prescale_dims)

        aff_tf_params = blob[self.tf_key]
        aff_tf = imgjit.build_affine_tf(**aff_tf_params)

        # post-scaling is conceptually done after the cropping,
        # in practice I do it before to do everything at once
        postscale_tf = imgjit.build_scale_tf(self.crop_to_dims, self.post_scale_dims)

        # centers things so that at the end the crop comes out right
        precrop_tf = imgjit.build_precrop_tf(prescale_dims, self.crop_to_dims)

        # a single affine transform
        # out_tf = postscale_tf+precrop_tf+uncenter_tf+aff_tf+center_tf+prescale_tf
        out_tf = (postscale_tf+precrop_tf)+(uncenter_tf+(aff_tf+center_tf))+prescale_tf

        if self.debug:
            blob['dbg'] = {}
            blob['dbg']['prescale_tf'] = prescale_tf.params
            blob['dbg']['center_tf'] = center_tf.params
            blob['dbg']['postscale_tf'] = postscale_tf.params
            blob['dbg']['precrop_tf'] = precrop_tf.params

        blob[kout] = {'w': self.post_scale_dims.width,
                      'h': self.post_scale_dims.height,
                      'matrix': out_tf.params}


class GenerateRandomCrop(ParafinaOp):

    rows_range = tr.Tuple((227, 448))
    shape_ratio = tr.Tuple((1, 1))
    mapping_default = {'rgb_image': 'rcrop'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]

            # if crop is too big simply specify max crop possible (whole image)
            # TODO maybe mirror/constant pad?
            if img.shape[0] < self.rows_range[0]:
                # log.warn('image is too small (%d), crop is no-op', img.shape[0])
                # blob.clear()
                blob[kout] = ((0, img.shape[0]), (0, img.shape[1]))
                continue

            # max row for random sample
            max_rows = min(img.shape[0], self.rows_range[1])
            # sample crop height
            height = np.random.randint(self.rows_range[0], max_rows+1)
            # compute width corresponding to shape_ratio
            ratio = float(self.shape_ratio[1])/self.shape_ratio[0]  # width/height
            width = int(np.floor(height*ratio))

            # if crop is too big simply specify max crop possible (whole image)
            if (img.shape[1] <= width or
                    img.shape[0] <= height):
                # log.warn('image is too small (%d), crop is no-op', img.shape[0])
                # print('image is too small (%d), crop is no-op' % img.shape[0])
                # blob.clear()
                blob[kout] = ((0, img.shape[0]), (0, img.shape[1]))
                continue

            # do the crop
            row0 = int(np.random.randint(0, img.shape[0]-height))
            col0 = int(np.random.randint(0, img.shape[1]-width))
            row1 = int(row0 + height)
            col1 = int(col0 + width)
            blob[kout] = ((row0, row1), (col0, col1))


class GenerateRandomEdgeCrop(ParafinaOp):
    """ crop out either sides or top-bottom randomly.
    sort of an inverse letterbox (pan-and-scan?)
    """

    crop_lr_ud = tr.CUnicode('lr')
    side_size = tr.Any()
    mapping_default = {'rgb_image': 'rcrop'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]

            if self.crop_lr_ud == 'lr':
                if self.side_size == 'max':
                    # so output is square
                    width = img.shape[0]
                else:
                    width = self.side_size
                row0 = 0
                row1 = img.shape[0]
                col0 = int(np.random.randint(0, img.shape[1]-width))
                col1 = int(col0 + width)
            else:
                if self.side_size == 'max':
                    # so output is square
                    height = img.shape[1]
                else:
                    height = self.side_size
                row0 = int(np.random.randint(0, img.shape[1]-height))
                row1 = int(row0 + height)
                col0 = 0
                col1 = img.shape[1]
            blob[kout] = ((row0, row1), (col0, col1))



class ApplyCrop(ParafinaOp):
    post_scale_shape = tr.Any()
    interp = tr.CUnicode('linear')
    mapping_default = {('rgb_image', 'rcrop'): 'rgb_image'}

    def _post_init(self):
        self._interp = get_cv2_interp_code(self.interp)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin[0]]
            row_slice, col_slice = blob[kin[1]]
            crop_img = img[slice(*row_slice), slice(*col_slice), ...]
            if self.post_scale_shape is not None:
                h, w = self.post_scale_shape
                crop_img = cv2.resize(crop_img, (w, h), None, 0, 0, self._interp)
            blob[kout] = crop_img


class SimpleRandomRescaleCrop(ParafinaOp):
    """ First rescales, then crops.
    Rescales (aspect-preserving) so smallest side is min_size.
    Then random crops with shape_ratio so height is rows.
    Optionally, scales output again to desired size.
    Ideally, avoid this option as it scales twice.
    """
    min_size = tr.Int(0)
    rows_range = tr.Tuple((227, 448))
    shape_ratio = tr.Tuple((1, 1))
    post_rows = tr.Int(0)
    flip = tr.Bool(True)
    interp = tr.CUnicode('linear')
    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        self._interp = get_cv2_interp_code(self.interp)

    def _update(self, blob):

        # to ensure same rng's apply to all items
        rnd_state = np.random.get_state()

        for kin, kout in self.mapping.items():

            np.random.set_state(rnd_state)

            img = blob[kin]

            if self.min_size > 0:
                if img.shape[0] < img.shape[1]:
                    img = aspect_preserving_scale(img, w=None, h=self.min_size, interp=self._interp)
                else:
                    img = aspect_preserving_scale(img, w=self.min_size, h=None, interp=self._interp)

            # crop dims
            height = np.random.randint(self.rows_range[0], self.rows_range[1]+1)
            ratio = float(self.shape_ratio[1])/self.shape_ratio[0]  # width/height
            width = int(np.floor(height*ratio))

            row0 = int(np.random.randint(0, img.shape[0]-height))
            col0 = int(np.random.randint(0, img.shape[1]-width))
            row1 = int(row0 + height)
            col1 = int(col0 + width)
            row_slice = (row0, row1)
            col_slice = (col0, col1)

            img = img[slice(*row_slice), slice(*col_slice), ...]

            if self.post_rows > 0:
                img = aspect_preserving_scale(img, None, self.post_rows, self._interp)

            if self.flip and np.random.random() < .5:
                img = img[:, ::-1, ...]

            blob[kout] = img


class FlipLR(ParafinaOp):
    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        # Note: same flipping is applied to all items
        if np.random.random() < 0.5:
            for kin, kout in self.mapping.items():
                img = blob[kin]
                img = img[:, ::-1, ...].copy()
                blob[kout] = img


class SimpleRescaleCrop(ParafinaOp):
    """ First rescales, then crops.
    Rescales (aspect-preserving) so smallest side is min_size.
    Then center crops with shape_ratio so height is rows.
    Optionally, scales output again to desired size.
    Ideally, avoid this option as it scales twice.
    """

    min_size = tr.Int(0)
    rows = tr.Int(448)
    shape_ratio = tr.Tuple((1, 1))
    post_rows = tr.Int(0)
    interp = tr.CUnicode('linear')
    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        self._interp = get_cv2_interp_code(self.interp)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]

            if self.min_size > 0:
                # height < width; rescale image so height = min_size
                if img.shape[0] < img.shape[1]:
                    img = aspect_preserving_scale(img, w=None, h=self.min_size, interp=self._interp)
                # height > width; rescale image so width = min_size
                else:
                    img = aspect_preserving_scale(img, w=self.min_size, h=None, interp=self._interp)

            # choose crop rectangle with desired aspect ratio
            height = self.rows
            ratio = float(self.shape_ratio[1])/self.shape_ratio[0]  # width/height
            width = int(np.floor(height*ratio))

            # crop from center
            row0 = int(np.floor((img.shape[0]-height)/2.0))
            col0 = int(np.floor((img.shape[1]-width)/2.0))
            row1 = int(row0 + height)
            col1 = int(col0 + width)
            row_slice = (row0, row1)
            col_slice = (col0, col1)
            img = img[slice(*row_slice), slice(*col_slice), ...]

            # if desired, scale to arbitrary shape.
            # avoid rescaling (again) if possible.
            if self.post_rows > 0 and self.post_rows != img.shape[0]:
                img = aspect_preserving_scale(img, w=None, h=self.post_rows, interp=self._interp)
            blob[kout] = img
