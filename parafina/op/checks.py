import numpy as np
import traitlets as tr

from .base import ParafinaOp


class LabelBounds(ParafinaOp):

    min_label = tr.Int(0)
    max_label = tr.Int(128)
    mapping_default = {'labels_image': ''}

    def _update(self, blob):
        for kin in self.mapping.keys():
            inp = blob[kin]
            assert(np.all(inp >= self.min_label))
            assert(np.all(inp < self.max_label))
