import numpy as np
import traitlets as tr

from .base import ParafinaOp


class DisparityToLogDepth(ParafinaOp):
    """
    TODO: assuming 01c ordering
    """

    use_log10 = tr.Bool(True)
    max_range = tr.Float(400.)
    mapping_default = {'disparity_image': 'log_depth_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            disp = blob[kin]
            # assuming uint16
            # note this will give runtime warning div by 0.
            # should they be handled beforehand?
            # both zero and max disparity seem invalid
            # TODO configure maybe
            is_zero = (disp == 0)
            is_max = (disp == 2**16-1)
            # avoid divide by zero
            disp[is_zero] = 1
            depth = (2**16-1)/disp.astype(np.float)

            if self.max_range > 0:
                depth = depth.clip(0., self.max_range)

            if self.use_log10:
                log_depth = np.log10(depth)
            else:
                log_depth = np.log(depth)

            # TODO shouldnt happen
            # bad = np.isnan(log_depth) | np.isinf(log_depth)
            # log_depth[bad] = 0
            # assert(not np.any(np.isnan(log_depth)))
            # assert(not np.any(np.isinf(log_depth)))

            log_depth = log_depth.astype('f4')
            log_depth[is_zero | is_max] = np.nan
            blob[kout] = log_depth


class DisparityToDepth(ParafinaOp):
    # value st that -Tx/disp = depth
    Tx = tr.Float(-1.)
    max_depth = tr.Float(200.)
    min_depth = tr.Float(0.)

    mapping_default = {'disparity_image': 'depth_image'}

    def _update(self, blob):
        for kin, kout in self.mapping_items():
            disp = blob[kin].copy()
            is_zero = (disp == 0)
            disp[is_zero] = 1
            depth = -self.Tx/disp
            if self.max_range > 0:
                gt_max = (depth > self.max_range)
            lt_min = (depth < self.min_range)
            invalid_mask = (is_zero | gt_max | lt_min)
            depth[invalid_mask] = 0.
            blob[kout] = depth


class DepthToDisparity(ParafinaOp):
    # value st that -Tx/disp = depth
    Tx = tr.Float(-1.)

    mapping_default = {'depth_image': 'disparity_image'}

    def _update(self, blob):
        for kin, kout in self.mapping_items():
            depth = blob[kin].copy()
            is_zero = (depth == 0)
            depth[is_zero] = 1.
            disp = -self.Tx/depth
            disp[is_zero] = 0.
            blob[kout] = disp
