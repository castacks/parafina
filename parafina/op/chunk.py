from copy import deepcopy

import traitlets as tr

from .base import ParafinaOp


class Chunk(ParafinaOp):
    """ just an idea. do not use. """

    chunk_size = tr.Int(4)

    def _post_init(self):
        self.buffer = []

    def _update(self, blob):
        self.buffer.append(deepcopy(blob))
        blob.clear()

        if len(self.buffer) == self.chunk_size:
            blob['chunk'] = self.buffer
            self.buffer = []
