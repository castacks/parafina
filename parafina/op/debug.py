from __future__ import print_function

import traitlets as tr

from .base import ParafinaOp


class PdbTrace(ParafinaOp):

    def _update(self, blob):
        import pdb
        pdb.set_trace()


class IpdbTrace(ParafinaOp):

    def _update(self, blob):
        import ipdb
        ipdb.set_trace()


class Print(ParafinaOp):

    def _update(self, blob):
        for kin, _ in self.mapping.items():
            print('{}: {}'.format(kin, blob[kin]))


class PyDisp(ParafinaOp):

    win_label = tr.Unicode('')
    to_bgr = tr.Bool(True)
    dtype = tr.Unicode('uint8')

    def _update(self, blob):
        import pydisp
        for kin, kout in self.mapping.items():
            # win, title, labels, width
            if len(self.win_label) > 0:
                lbltxt = blob[self.win_label]
                labels = [[0.02, 0.02, lbltxt]]
            else:
                labels = []
            img = blob[kin].astype(self.dtype)
            pydisp.image(img,
                         win=kout,
                         labels=labels,
                         title=kout,
                         to_bgr=self.to_bgr)
