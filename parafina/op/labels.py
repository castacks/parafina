import cv2
import numpy as np

import traitlets as tr

import skimage.morphology as skmorph

from ..util import remapping
from .base import ParafinaOp


class OrdinalCoding(ParafinaOp):
    """ re-encode 1-K to unary, e.g.
    1 -> [1, 0, 0, ...]
    2 -> [1, 1, 0, ...]
    3 -> [1, 1, 1, ...]
    """

    num_classes = tr.Int(0)
    mapping_default = {'labels': 'labels'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            label = blob[k_in]
            label_vec = np.zeros(self.num_classes-1, dtype='f4')
            label_vec[:label] = 1.
            blob[k_out] = label_vec


class LabelRemap(ParafinaOp):
    """ Scalar label mapping using a dictionary.

    Trying to map nonexistent keys will cause a KeyError.
    """

    label_mapping = tr.Dict()
    # default_label = tr.Any()
    mapping_default = {'labels': 'labels'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            label = blob[k_in]
            new_label = self.label_mapping[label]
            blob[k_out] = new_label


class LabelImageRemap(ParafinaOp):
    """ Remap labels in a label image.
    mapping from source labels to target labels is given in a dictionary,
    `label_mapping`. This label_mapping can be fixed or given in the blob.
    The latter can be useful for dynamic mappings, such as when combining
    datasets.
    """

    label_mapping = tr.Dict()
    strict = tr.Bool(True)
    dtype = tr.Any(None)
    # used if label_mapping is in blob
    label_mapping_key = tr.CUnicode()
    mapping_default = {'labels_image': 'labels_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            inp = blob[kin]
            # blob['labels_image_orig'] = inp.copy()
            if self.label_mapping_key:
                label_mapping = blob[self.label_mapping_key]
            else:
                label_mapping = self.label_mapping

            assert(len(label_mapping) > 0)

            outp = remapping.remap_labels(inp, label_mapping, default=-1)

            if self.strict and (outp < 0).any():
                raise ValueError('image {} has labels not present in label_mapping'.format(kin))

            if not self.strict:
                # replace -1 by 0
                outp = outp.clip(min=0)

            if self.dtype is not None:
                outp = outp.astype(self.dtype)

            blob[kout] = outp


class LabelImageToOneHot(ParafinaOp):
    """
    """

    num_classes = tr.Int(2)
    strict = tr.Bool(True)
    dtype = tr.Any(None)
    mapping_default = {'labels_image': 'labels_image'}

    def _post_init(self):
        self.one_hot_tab = np.eye(self.num_classes+1)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            inp = blob[kin]

            raise NotImplementedError()
            outp = remapping.remap_labels(inp, label_mapping, default=-1)

            if self.strict and (outp < 0).any():
                raise ValueError('image {} has labels not present in label_mapping'.format(kin))

            if not self.strict:
                # replace -1 by 0
                outp = outp.clip(min=0)

            if self.dtype is not None:
                outp = outp.astype(self.dtype)

            blob[kout] = outp


class DilateBinary(ParafinaOp):
    """ dilate binary labels.
    """

    pixels = tr.Int(10)
    mapping_default = {'labels_image': 'labels_image'}

    def _post_init(self):
        self.selem = skmorph.square(self.pixels)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            limg = blob[kin]
            # TODO consider 1-hot case
            if limg.ndim == 3:
                raise ValueError('images must have one channel')
            blob[kout] = skmorph.binary_dilation(limg, self.selem).astype('u1')


class DilateGray(ParafinaOp):
    """ dilate grayscale images. uses 'max' operator.
    """

    pixels = tr.Int(10)
    mapping_default = {'labels_image': 'labels_image'}

    def _post_init(self):
        self.selem = skmorph.square(self.pixels)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            limg = blob[kin]
            blob[kout] = skmorph.dilation(limg, self.selem)


class Blur(ParafinaOp):
    """ apply gaussian or box blur to image.
    """

    ksize = tr.Int(3)
    filter = tr.Unicode('box')
    mapping_default = {'labels_image': 'labels_image'}

    def _post_init(self):
        # TODO use traitlet validation mechanism
        assert(self.filter in ['box', 'gauss'])

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            limg = blob[kin]
            if self.filter == 'gauss':
                blob[kout] = cv2.GaussianBlur(limg, (self.ksize, self.ksize), 0)
            elif self.filter == 'box':
                blob[kout] = cv2.blur(limg, (self.ksize, self.ksize))
            else:
                raise ValueError('unknown filter')
