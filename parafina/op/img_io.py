import cStringIO as StringIO
import logging
from PIL import Image

import numpy as np
import traitlets as tr
import cv2
from spath import Path

from .base import ParafinaOp


log = logging.getLogger(__name__)


def translate_cv2_flags(str_flag):
    d = {'COLOR': cv2.IMREAD_COLOR,
         'UNCHANGED': cv2.IMREAD_UNCHANGED}
    return d[str_flag]


class LoadRawFile(ParafinaOp):

    cached = tr.Bool(False)
    mapping_default = {'rgb_fname': 'rgb_image'}

    def _post_init(self):
        if self.cached:
            self._cache = {}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            fname = blob[k_in]
            if self.cached and str(fname) in self._cache:
                # log.info('cache hit: %s cache: %s', fname, len(self._cache))
                blob[k_out] = self._cache[str(fname)][:]
                continue

            # log.info('cache miss: %s cache: %s', fname, len(self._cache))
            raw_file = Path(fname).bytes()
            blob[k_out] = raw_file

            if self.cached:
                self._cache[str(fname)] = raw_file[:]


class CvLoadImage(ParafinaOp):
    """ load images with opencv.
    note: images will be in bgr8.
    """

    strict = tr.Bool(False)
    flags = tr.CUnicode('UNCHANGED')
    cached = tr.Bool(False)
    mapping_default = {'rgb_fname': 'rgb_image'}

    def _post_init(self):
        if self.cached:
            self._cache = {}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            fname = blob[k_in]
            if self.cached and str(fname) in self._cache:
                # log.info('cache hit: %s cache: %s', fname, len(self._cache))
                blob[k_out] = self._cache[str(fname)].copy()
                continue

            # log.info('cache miss: %s cache: %s', fname, len(self._cache))
            img = cv2.imread(str(fname), translate_cv2_flags(self.flags))
            if self.strict and img is None:
                msg = 'error loading img: {}, len(cache): {}'.format(fname, len(self._cache))
                log.error(msg)
                raise IOError(msg)
            blob[k_out] = img

            if self.cached:
                self._cache[str(fname)] = img.copy()


class CvDecodeImage(ParafinaOp):
    """ load images with opencv.
    note: images will be in bgr8.
    """

    strict = tr.Bool(False)
    flags = tr.CUnicode('UNCHANGED')
    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            buf = np.frombuffer(blob[k_in], dtype='u1')
            img = cv2.imdecode(buf, translate_cv2_flags(self.flags))
            if self.strict and img is None:
                raise ValueError('error decoding image: {}'.format(k_in))
            blob[k_out] = img.copy()
            # log.info('cvdec %r', img.shape)


class PilDecodeLabelImage(ParafinaOp):
    """ load images with pil.
    """
    mapping_default = {'labels_image': 'labels_image'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            sio = StringIO.StringIO(blob[k_in])
            img = Image.open(sio)
            aimg = np.asarray(img)
            assert(aimg.size > 0)
            blob[k_out] = aimg
            sio.close()
            # log.info('pildec %r', aimg.shape)


class PilLoadLabelImage(ParafinaOp):
    """ load images with pil.
    """

    cached = tr.Bool(False)
    mapping_default = {'labels_fname' : 'labels_image'}

    def _post_init(self):
        if self.cached:
            self._cache = {}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            fname = blob[k_in]
            if self.cached and str(fname) in self._cache:
                # log.info('cache hit: %s cache: %s', fname, len(self._cache))
                blob[k_out] = self._cache[str(fname)].copy()
                continue

            # log.info('cache miss: %s cache: %s', fname, len(self._cache))
            fname = blob[k_in]
            img = Image.open(str(fname))
            aimg = np.asarray(img)
            blob[k_out] = aimg

            if self.cached:
                self._cache[str(fname)] = aimg.copy()


class RgbToBgr(ParafinaOp):
    # I guess it should be bgr_image... :/
    mapping_default = {'rgb_image': 'rgb_image'}

    def _update(self, blob):
        for k_in, k_out in self.mapping.items():
            img = blob[k_in]
            blob[k_out] = img[:, :, ::-1].copy()
