import numpy as np
import traitlets as tr

from .base import ParafinaOp


class RescaleNormals(ParafinaOp):
    """
    assuming 01c ordering
    """

    dtype = tr.CUnicode('float32')
    mapping_default = {'normals_image': 'normals_image'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            # two conventions for invalid: (0,0,0) and (127,127,127)
            invalid = np.all(blob[kin] == 0, 2) | np.all(blob[kin] == 127, 2)
            nimg = 2.0*(blob[kin]/255. - 0.5)
            # normalize because sometimes normalization gets skewed
            # slightly when undistorting
            norms = np.sqrt(np.sum(nimg**2, 2, keepdims=True))
            nimg /= norms
            nimg[invalid] = 0.
            blob[kout] = nimg
