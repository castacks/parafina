import numpy as np
import traitlets as tr

from .base import ParafinaOp


class CnnPreprocess(ParafinaOp):
    """
    """
    # this is default vgg mean pixel, rgb
    # sufficient for most purposes
    # set to none to avoid mean_px subtraction
    mean_px = tr.Any((123.51186371, 115.77290344, 102.71886444))
    to_c01 = tr.Bool(True)
    to_bgr = tr.Bool(False)
    dtype = tr.CUnicode('float32')
    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        if self.mean_px is not None:
            # ensure it's an array
            self.mean_px = np.asarray(self.mean_px, dtype='f4').reshape((3, 1, 1))

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]
            img = img.astype(self.dtype)
            if img.ndim == 3 and self.to_c01:
                img = np.rollaxis(img, 2)
            if self.mean_px is not None:
                img -= self.mean_px
            if self.to_bgr:
                if self.to_c01:
                    img = img[::-1, :, :]
                else:
                    img = img[:, :, ::-1]
            blob[kout] = img.copy()


class CnnPreprocessPyTorch(ParafinaOp):
    """
    preprocess consistently with pytorch model zoo.
    assumes RGB input.
    """
    mean_px = tr.Any((0.485, 0.456, 0.406))
    std_px = tr.Any((0.229, 0.224, 0.225))
    to_c01 = tr.Bool(True)
    to_bgr = tr.Bool(False)
    dtype = tr.CUnicode('float32')
    mapping_default = {'rgb_image': 'rgb_image'}

    def _post_init(self):
        if self.mean_px is not None:
            # ensure it's an array
            self.mean_px = np.asarray(self.mean_px, dtype='f4').reshape((3, 1, 1))
        if self.std_px is not None:
            self.std_px = np.asarray(self.std_px, dtype='f4').reshape((3, 1, 1))

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin]
            img = img.astype(self.dtype)
            img /= 255
            if img.ndim == 3 and self.to_c01:
                img = np.rollaxis(img, 2)
            if self.mean_px is not None:
                img -= self.mean_px
            if self.std_px is not None:
                img /= self.std_px
            if self.to_bgr:
                if self.to_c01:
                    img = img[::-1, :, :]
                else:
                    img = img[:, :, ::-1]
            blob[kout] = img.copy()


class CnnGrayPreprocess(ParafinaOp):
    mean_px = tr.Float(128.)
    dtype = tr.CUnicode('float32')
    # perhaps should rename key
    mapping_default = {'rgb_image': 'rgb_image'}
    to_bc01 = tr.Bool(True)

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            img = blob[kin].astype(self.dtype).copy()
            img -= self.mean_px
            if img.ndim == 2 and self.to_bc01:
                # to (H, W) -> (1, H, W)
                img = img[None, ...]
            blob[kout] = img


class HwcToNchw(ParafinaOp):
    mapping_default = {'x': 'x'}

    def _update(self, blob):
        for kin, kout in self.mapping.items():
            x = blob[kin].copy()
            if x.ndim > 2:
                # roll channel to first dim
                x = np.rollaxis(x, 2)
            else:
                # add first singleton dim
                x = x[None, ...]
            blob[kout] = x
