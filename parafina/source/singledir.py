from collections import OrderedDict
import random

from funcy import cached_property
from spath import Path

import logging

log = logging.getLogger(__name__)


class SingleDirSrc(object):
    """
    Generate instances for a bunch of images
    in a single directory.

    Files for images and for labels are specified
    with fname_patterns. Correspondence is determined
    by sorting each list of files.

    Ex. base_dir foo has 2 instances:
        foo/rgb01.jpg
        foo/rgb02.jpg
        foo/labels01.jpg
        foo/labels02.jpg
    fnames_patterns can be {'rgb': 'rgb*.jpg', 'labels': 'labels*.jpg'}
    Correspondence between rgb01 and labels01 is done by sorting.
    """

    def __init__(self,
                 base_dir,
                 fname_patterns,
                 shuffle=True):
        self.base_dir = Path(base_dir)
        self.fname_patterns = fname_patterns
        self.shuffle = shuffle

    @cached_property
    def fnames_dict(self):
        fnames_dict = OrderedDict()
        for fname_key, fname_pattern in self.fname_patterns.items():
            fnames_dict[fname_key] = sorted(self.base_dir.files(fname_pattern))

        # verify we have 1-1 correspondence
        lengths = map(len, fnames_dict.values())
        if len(set(lengths)) > 1:
            raise ValueError('not all fname patterns have same length')
        log.info('found %d instances' % len(fnames_dict.values()[0]))
        return fnames_dict

    def __iter__(self):
        def gen():
            instances = []
            for fnames in zip(*self.fnames_dict.values()):
                assert(len(fnames) == len(self.fnames_dict.keys()))
                for fname_key, fname in zip(self.fnames_dict.keys(), fnames):
                    instance = OrderedDict()
                    instance['base_dir'] = self.base_dir
                    instance['instance_dir'] = fname.parent
                    instance[fname_key] = fname
                    instances.append(instance)

            if self.shuffle:
                random.shuffle(instances)

            for instance in instances:
                yield instance
        return gen()
