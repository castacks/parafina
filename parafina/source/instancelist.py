"""
Generates instances.
For instance-per-directory data format.
"""

import json
import os
import logging
import random
import fnmatch
from collections import OrderedDict

try:
    # faster in py2; in py3, scandir.walk is same as os.walk
    from scandir import walk
except ImportError:
    from os import walk

from funcy import cached_property

from spath import Path


IGNORE_FNAME = '.datasetignore'


log = logging.getLogger(__name__)


class InstanceListSrc(object):
    """ Directory per instance format.
    first find using include_pattern, then filter using exclude_pattern.
    fname_mapping: a mapping of filenames to load, e.g.
    ('rgb_fname': 'rgb.jpg', 'labels_fname': 'labels.jpg')
    split_key: assuming json file, structured as dictionary
    """

    def __init__(self,
                 base_dir,
                 instance_list_fname,
                 split_key,
                 fname_mapping,
                 shuffle=True,
                 ignore_missing_files=False):

        self.base_dir = Path(base_dir)
        self.fname_mapping = fname_mapping
        self.shuffle = shuffle
        self.ignore_missing_files = ignore_missing_files

        splits = (base_dir/instance_list_fname).read_json()
        self.idirs = splits[split_key]

    def __iter__(self):
        # make a new generator each time
        def gen():
            log.info('starting instance gen with %d dirs', len(self.idirs))
            if self.shuffle:
                log.info('shuffling')
                random.shuffle(self.idirs)

            for idir in self.idirs:
                skip = False
                instance = OrderedDict()
                instance['base_dir'] = self.base_dir
                instance['instance_dir'] = idir
                for fname_key, fname_val in self.fname_mapping.items():
                    full_fname = (self.base_dir/idir)/fname_val
                    if self.ignore_missing_files and not full_fname.exists():
                        # log.info('skipping {} for missing {}'.format(idir, fname_val))
                        skip = True
                        break
                    instance[fname_key] = full_fname
                if skip:
                    del instance
                    continue
                yield instance
        return gen()
