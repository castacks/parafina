import cPickle as pickle
import logging
import copy


log = logging.getLogger(__name__)


class PickledInstanceDirSrc(object):
    """ Generate instances from a pickled file.
    """

    def __init__(self,
                 fname,
                 cached=True):
        self.fname = fname
        self.cached = cached

    def stream_from_disk(self):
        # stream straight from disk, will not save in memory
        log.info('unpacking blobs')
        with open(self.fname, 'rb') as f:
            while True:
                try:
                    blob = pickle.load(f)
                    yield blob
                except EOFError:
                    break
        log.info('done unpacking blobs')

    def stream_from_memory(self):
        # save copy in memory so it's faster next time
        # won't work for super large datasets
        if hasattr(self, 'blobs'):
            log.info('streaming from memory')
            for blob in self.blobs:
                yield copy.deepcopy(blob)
        else:
            self.blobs = []
            for blob in self.stream_from_disk():
                self.blobs.append(blob)
                yield copy.deepcopy(blob)

    def __iter__(self):
        if self.cached:
            return self.stream_from_memory()
        else:
            return self.stream_from_disk()
