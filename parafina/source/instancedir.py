"""
Generates instances.
For instance-per-directory data format.
"""

import os
import logging
import random
import fnmatch
from collections import OrderedDict

try:
    # faster in py2; in py3, scandir.walk is same as os.walk
    from scandir import walk
except ImportError:
    from os import walk

from funcy import cached_property

from spath import Path


IGNORE_FNAME = '.datasetignore'


log = logging.getLogger(__name__)


class InstanceDirSrc(object):
    """ Directory per instance format.
    first find using include_pattern, then filter using exclude_pattern.
    fnames: a mapping of fnames to load, e.g.
    ('rgb_fname': 'rgb.jpg', 'labels_fname': 'labels.jpg')

    """

    def __init__(self,
                 base_dir,
                 fnames,
                 shuffle=True,
                 include_pattern=None,
                 exclude_pattern=None,
                 ignore_missing_files=False):

        self.base_dir = Path(base_dir)
        self.fnames = fnames
        self.shuffle = shuffle
        if include_pattern == '' or include_pattern == '*':
            include_pattern = None
        if exclude_pattern == '':
            exclude_pattern = None
        self.include_pattern = include_pattern
        self.exclude_pattern = exclude_pattern
        self.ignore_missing_files = ignore_missing_files

    @cached_property
    def idirs(self):
        idirs = []
        for d, subdirs, fnames in walk(self.base_dir, followlinks=True):
            if IGNORE_FNAME in fnames:
                # skip dir and subdirs
                del subdirs[:]
                continue
            # by convention, ignore metadata dirs
            if 'metadata' in subdirs:
                subdirs.remove('metadata')
            if len(fnames) == 0:
                continue
            d_basename = os.path.basename(d)
            if (((self.include_pattern is None) or
                 fnmatch.fnmatch(d_basename, self.include_pattern)) and
                    ((self.exclude_pattern is None) or not
                     fnmatch.fnmatch(d_basename, self.exclude_pattern))):
                idirs.append(Path(d))
        log.info('found %d dirs', len(idirs))

        if not self.shuffle:
            idirs = sorted(idirs)
        return idirs

    def __iter__(self):
        # make a new generator each time
        def gen():
            log.info('starting instance gen with %d dirs', len(self.idirs))
            if self.shuffle:
                log.info('shuffling')
                random.shuffle(self.idirs)

            for idir in self.idirs:
                skip = False
                instance = OrderedDict()
                instance['base_dir'] = self.base_dir
                instance['instance_dir'] = idir
                for fname_key, fname_val in self.fnames.items():
                    full_fname = idir/fname_val
                    if self.ignore_missing_files and not full_fname.exists():
                        # log.info('skipping {} for missing {}'.format(idir, fname_val))
                        skip = True
                        break
                    instance[fname_key] = full_fname
                if skip:
                    del instance
                    continue
                yield instance
        return gen()
