""" Requires pyav library.



"""

from collections import OrderedDict
import logging

import numpy as np
import av

from spath import Path

log = logging.getLogger(__name__)


class VideoFrameSrc(object):
    """ Generate frames using pyav.
    Loads image data directly.
    """

    def __init__(self,
                 base_dir,
                 video_fname,
                 fps=-1,
                 out_key='rgb_image'):
        self.base_dir = Path(base_dir)
        self.video_fname = Path(video_fname)
        self.fps = fps
        self.out_key = out_key

    def __iter__(self):
        # make a new generator each time
        def gen():
            log.info('starting video gen from %s', self.video_fname)
            instance = OrderedDict()
            instance['base_dir'] = self.base_dir

            container = av.open(str(self.base_dir/self.video_fname))
            video = next(s for s in container.streams if s.type == b'video')
            actual_fps = float(video.average_rate)

            if self.fps > 0:
                every_nth = max(1, int(actual_fps/float(self.fps)))
            else:
                every_nth = 1

            for packet in container.demux(video):
                for frame in packet.decode():
                    if frame.index % every_nth != 0:
                        continue
                    # img_orig = frame.to_image()
                    # img = np.asarray(img_orig)
                    img = frame.to_nd_array(format='bgr24')
                    blob = {}
                    blob['video_fname'] = self.video_fname
                    blob['frame_ix'] = frame.index
                    blob['average_rate'] = actual_fps
                    blob[self.out_key] = img
                    yield blob
        return gen()

if __name__ == '__main__':
    gen = VideoFrameSrc('/mnt/dms/mavs_ws/yt2_qual0', '-2f-rX3wp7o.mp4')
    for blob in gen:
        print blob


