import cPickle as pickle
import tarfile
import fnmatch

class PickleTarSrc(object):
    """ Direct reading of tar'd, possibly compressed pickles.
    By default, read in streaming mode,
    faster when you have lots of small files.
    """
    def __init__(self, fname, pattern, mode='r|'):
        self.fname = fname
        self.mode = mode
        self.pattern = pattern

    def __iter__(self):
        def gen():
            tfile = tarfile.open(self.fname, mode=self.mode)
            for entry in tfile:
                if not fnmatch.fnmatch(entry.name, self.pattern):
                    continue
                fhandle = tfile.extractfile(entry)
                blob = pickle.load(fhandle)
                blob['tarname'] = entry.name
                # buf = zlib.decompress(fileobj.read())
                # blob = pickle.load(StringIO.StringIO(buf))
                yield blob
            tfile.close()
        return gen()
