# -*- coding: utf-8 -*-

"""Top-level package for parafina."""

__author__ = """Daniel Maturana"""
__email__ = 'dimatura@cmu.edu'
__version__ = '0.1.0'
