
from collections import namedtuple
import numpy as np

import skimage.transform as skimgtf
import cv2

from .img_dims import ImgDims

TransformParams = namedtuple('TransformParams',
        ['zoom', 'rotation', 'translation', 'flip'])


identity_transform = TransformParams(
        zoom=(1., 1.),
        rotation=0,
        translation=(0, 0),
        flip=False)


def build_precrop_tf(src_dims, dst_dims):
    """ shifting to a new image center;
    useful before a crop to dst_dims.
    """
    ishift_x = (src_dims.width-dst_dims.width)/2.0
    ishift_y = (src_dims.height-dst_dims.height)/2.0
    # center img in image coordinates before a crop. (center h/2, w/2)
    # crop can be done passing output_shape=(dst_rows, dst_cols) to skimgtf.warp
    icenter_tf = skimgtf.SimilarityTransform(translation=(ishift_x, ishift_y))
    return icenter_tf


def build_center_uncenter_tfs(img_dims):
    """ shift to center around (0, 0).
    used to ensure that zooming and rotation happens around the center of the
    image
    """
    # center img in 'logical' coordinates (center at 0,0)
    lshift_x = img_dims.width/2.0 - 0.5
    lshift_y = img_dims.height/2.0 - 0.5
    lcenter_tf = skimgtf.SimilarityTransform(translation=(lshift_x, lshift_y))
    lcenter_inv_tf = skimgtf.SimilarityTransform(translation=(-lshift_x, -lshift_y))
    return lcenter_tf, lcenter_inv_tf


def build_scale_tf(src_dims, dst_dims):
    sx = float(src_dims.width)/dst_dims.width
    sy = float(src_dims.height)/dst_dims.height
    simtf = skimgtf.SimilarityTransform(scale=(sx, sy))
    return simtf


def build_aspect_ratio_preserving_tf(src_dims, dst_dims):
    """ infers w or h to preserve aspect ratio """
    w = dst_dims.width
    h = dst_dims.height
    assert not ((w is None) and (h is None))
    ratio = float(src_dims.width)/float(src_dims.height)
    # print src_dims, dst_dims
    # print 'w:', w, 'h:', h, 'ratio:', ratio
    if h is None:
        h = (float(w)/ratio)
    elif w is None:
        w = (float(h)*ratio)
    sx = float(src_dims.width)/w
    sy = float(src_dims.height)/h
    # print 'w:', w, 'h:', h, 'sx:', sx, 'sy:', sy
    return ImgDims(w, h), skimgtf.SimilarityTransform(scale=(sx, sy))


def build_aspect_ratio_preserving_tf2(src_dims, dst_dims):
    """ infers w or h to preserve aspect ratio """
    w = dst_dims.width
    h = dst_dims.height
    ratio = float(src_dims.width)/float(src_dims.height)
    # print src_dims, dst_dims
    if src_dims.height > src_dims.width:
        # if height is larger than width change it
        h = (float(w)/ratio)
    else:
        w = (float(h)*ratio)
    sx = float(src_dims.width)/w
    sy = float(src_dims.height)/h
    # print 'w:', w, 'h:', h, 'sx:', sx, 'sy:', sy
    return ImgDims(w, h), skimgtf.SimilarityTransform(scale=(sx, sy))


def build_affine_tf(zoom=(1.0, 1.0), rotation=0, translation=(0, 0), flip=False):
    """
    build skimg affine tf
    """
    if flip:
        zoom = (-zoom[0], zoom[1])
    scale = (1./zoom[0], 1./zoom[1])
    rot = np.deg2rad(rotation)
    aff_tf = skimgtf.AffineTransform(scale=scale,
                                     rotation=rot,
                                     translation=translation)
    return aff_tf


class Warper(object):

    def __init__(self,
                 aff_tf_params,
                 pre_scale_shape=None,
                 crop_to_shape=None,
                 post_scale_shape=None,
                 interp=cv2.INTER_CUBIC):
        """
        (conceptual) operations:
        - center around origin
        - apply affine tf (rotation/zoom/shift)
        - undo centering
        - crop to crop_to_shape
        - scale to post_scale_shape
        NOTE shapes are (rows, cols) :/ might change
        TODO
        support c01 again
        """
        self.aff_tf_params = aff_tf_params
        self.pre_scale_shape = pre_scale_shape
        self.post_scale_dims = ImgDims.from_shape(pre_scale_shape)
        self.crop_to_dims = ImgDims.from_shape(crop_to_shape)
        self.interp = interp
        self.post_scale_dims = ImgDims.from_shape(post_scale_shape)
        self.aff_tf = build_affine_tf(*self.aff_tf_params)

    def get_tf(self, img_shape):
        """ computes the actual tf to apply.
        depends on input image dimensions.
        """
        img_dims = ImgDims.from_shape(img_shape)
        precrop_tf = build_precrop_tf(img_dims, self.crop_to_dims)
        center_tf, uncenter_tf = build_center_uncenter_tfs(img_dims)
        simtf = build_scale_tf(self.crop_to_dims, self.post_scale_dims)
        # note: this is the INVERSE warp. so actual
        # transforms are applied left to right, inverted.
        newtf = simtf+precrop_tf+uncenter_tf+self.aff_tf+center_tf
        return newtf

    def warp(self, img):
        """ applies warp to image.
        note actual warp depends on input image dimensions.
        """
        assert(img.ndim == 2 or img.ndim == 3)

        warptf = self.get_tf(img.shape)
        # note cv2 seems faster than skimage.
        # also, returns in uint8 or float.
        # tell cv2 we are giving it the inverse map.
        flags = self.interp | cv2.WARP_INVERSE_MAP
        dsize = tuple((self.post_scale_dims.width,
                       self.post_scale_dims.height))
        wimg = cv2.warpAffine(img,
                              warptf.params[:2],
                              dsize,
                              None,
                              flags)
        return wimg


    def unwarp(self, img):
        """ TODO does not work
        """
        warptf = self.get_tf(img.shape)
        # note cv2 seems faster than skimage.
        # also, returns in uint8 or float.
        flags = self.interp
        dsize = tuple((self.post_scale_dims.width,
                       self.post_scale_dims.height))
        wimg = cv2.warpAffine(img,
                              warptf.params[:2],
                              dsize,
                              None,
                              flags)
        return wimg


class AffineTfParamsSampler(object):
    def __init__(self, zoom_range=(1 / 1.1, 1.1),
                       rotation_range_deg=(-5, 5),
                       translation_range_px=((-4, 4), (-4, 4)),
                       do_flip=True,
                       allow_stretch=False,
                       rng=None):
        self.zoom_range = zoom_range
        self.rotation_range_deg = rotation_range_deg
        self.translation_range_px = translation_range_px
        self.do_flip = do_flip
        self.allow_stretch = allow_stretch
        if rng is not None:
            self.rng = rng
        else:
            self.rng = np.random.RandomState()

    def __call__(self, jitter=True):
        if jitter:
            return self._sample()

    def _sample(self):
        translation = (self.rng.uniform(*self.translation_range_px[0]),
                       self.rng.uniform(*self.translation_range_px[1]))
        rotation = self.rng.uniform(*self.rotation_range_deg)
        if self.do_flip:
            flip = (self.rng.randint(2) > 0)
        else:
            flip = False
        # random zoom
        log_zoom_range = np.log(np.asarray(self.zoom_range))
        if isinstance(self.allow_stretch, float):
            log_stretch_range = [-np.log(self.allow_stretch), np.log(self.allow_stretch)]
            zoom = np.exp(self.rng.uniform(*log_zoom_range))
            stretch = np.exp(self.rng.uniform(*log_stretch_range))
            zoom_x = zoom * stretch
            zoom_y = zoom / stretch
        elif self.allow_stretch is True:  # avoid bugs, f.e. when it is an integer
            zoom_x = np.exp(self.rng.uniform(*log_zoom_range))
            zoom_y = np.exp(self.rng.uniform(*log_zoom_range))
        else:
            zoom_x = zoom_y = np.exp(self.rng.uniform(*log_zoom_range))
        # return ((zoom_x, zoom_y), rotation, translation, flip)
        return TransformParams(zoom=(zoom_x, zoom_y), rotation=rotation,
                               translation=translation, flip=flip)
