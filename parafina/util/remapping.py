from collections import OrderedDict
from PIL import Image
import numpy as np


def rgb_image_to_label_image(img, mapping, default=0):
    """ Map RGB image to label image.

    :parameters:
        - img: HxWx3 uint8 image (numpy array)
        - mapping: dictionary of (r,g,b) -> int
        - default: label to use for keys not in mapping
    """
    if img.ndim != 3:
        raise ValueError('img must be HxWx3 matrix')
    if img.shape[2] != 3:
        raise ValueError('img must be HxWx3 matrix')

    img2 = img.astype('uint32')

    # assign a unique integer to each (r, g, b) tuple
    rshift = img2[:,:,0]<<16
    gshift = img2[:,:,1]<<8
    bshift = img2[:,:,2]
    imgh = np.bitwise_or(np.bitwise_or(rshift, gshift), bshift)

    # create a big table with one entry for each 256**3-1 colors
    # 16777216 == 255*(2**16) + 255*(2**8) + 255 + 1 == 256**3
    rgb_tab = np.zeros(16777216, dtype='uint32')
    if default != 0:
        rgb_tab.fill(default)

    # fill table with labels in mapping
    for k, v in mapping.iteritems():
        hk = k[0]*(2**16) + k[1]*(2**8) + k[2]
        rgb_tab[hk] = v

    # take() is slightly faster
    # limg2 = rgb_tab[imgh]
    limg2 = np.take(rgb_tab, imgh)
    return limg2


def label_image_to_rgb_image(label_img, mapping, default=[255, 255, 255]):
    """ Map integer label image to rgb image.

    :parameters:
        - label_img: int image
        - mapping: dictionary of int -> (r, g, b)
        - default: default rgb
    """
    lbl_max = np.max(mapping.keys())
    label_tab = np.empty((lbl_max+1, 3), dtype='u1')
    label_tab[:] = default
    for lbl_src, lbl_dst in mapping.iteritems():
        label_tab[lbl_src] = lbl_dst
    return label_tab[label_img]



def rgb_image_to_onehot_image(img, mapping, num_labels):
    img2 = img.astype('uint32')
    imgh = np.bitwise_or(np.bitwise_or((img2[:,:,0]<<16), (img2[:,:,1]<<8)), (img2[:,:,2]))
    # 16777216 == 255*(2**16) + 255*(2**8) + 255 + 1 == 256**3
    rgb_tab = np.zeros((16777216, num_labels+1), dtype='uint32')
    # explicitly add 0 entry, st (0, 0, 0) -> 0 (one-hot encoded)
    # TODO configurable
    rgb_tab[0, 0] = 1
    for k,v in mapping.items():
        hk = k[0]*(2**16) + k[1]*(2**8) + k[2]
        rgb_tab[hk, v] = 1
    limg2 = rgb_tab[imgh]
    # c01 ordering
    limg2 = limg2.transpose((2, 0, 1))
    return limg2


def remap_labels(label_img, mapping, default=-1):
    """ Map integer labels to integer labels.

    :parameters:
        - label_img: int image
        - mapping: dictionary of int -> int
        - default: default int
    """
    lbl_max = np.max(mapping.keys())
    label_tab = np.empty((lbl_max+1), dtype='int64')
    label_tab.fill(default)
    for lbl_src, lbl_dst in mapping.iteritems():
        label_tab[lbl_src] = lbl_dst
    return label_tab[label_img]


def add_color_palette(img, class_id_to_rgb):
    """ Add color palette to 8-bit PIL image.

    :parameters:
        - `class_id_to_rgb`: dict or list.
        Colors must be integers in (0, 255) range.
    """
    if not isinstance(img, Image.Image):
        img = Image.fromarray(img)
        if img.mode == 'RGB':
            raise ValueError('Invalid image mode (RGB)')

    if (isinstance(class_id_to_rgb, dict) or
        isinstance(class_id_to_rgb, OrderedDict)):
        class_id_to_rgb_lst = []
        keys = sorted(class_id_to_rgb.keys())
        max_key = np.max(keys)
        for k in xrange(max_key+1):
            rgb = class_id_to_rgb.get(k, (0, 0, 0))
            class_id_to_rgb_lst.append(rgb)
    else:
        class_id_to_rgb_lst = class_id_to_rgb

    class_id_to_rgb_lst_flat = []
    for rgb in class_id_to_rgb_lst:
        class_id_to_rgb_lst_flat.extend(list(rgb))

    img.putpalette(class_id_to_rgb_lst_flat)
    return img
